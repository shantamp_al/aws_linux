# This is the commands that will install Apache on our EC2 instances and use our html file as front page.

# Update VM server
sudo yum Update

# Install Apache
sudo yum install -y httpd.x86_64
sudo systemctl start httpd.service
sudo systemctl enable httpd.service

# Replace Apache test page with own index.html
sudo mv ~/shannon_aws_website/index.html  /var/www/html

# Restart Apache to check own page is running
sudo systemctl restart httpd.service